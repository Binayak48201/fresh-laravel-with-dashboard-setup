<?php

use Illuminate\Support\Facades\Route;

require __DIR__ . '/auth.php';

Route::group(['middleware' => 'auth:sanctum'], function () {
    Route::get('/dashboard', function () {
        return view('dashboard');
    })->name('dashboard');

    // We can create routes from here which is only access by super_admin.
    Route::group(['middleware' => ['role:super_admin']], function () {

    });
});


