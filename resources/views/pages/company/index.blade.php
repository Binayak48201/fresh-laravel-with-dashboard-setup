@push('links')
    <link rel="stylesheet" type="text/css" href="duluxs/vendors/css/forms/wizard/bs-stepper.min.css">
    <link rel="stylesheet" type="text/css" href="duluxs/vendors/css/forms/select/select2.min.css">
    <link rel="stylesheet" type="text/css" href="duluxs/css/plugins/forms/form-validation.css">
    <link rel="stylesheet" type="text/css" href="duluxs/css/plugins/forms/form-wizard.css">
@endpush
@extends('layouts.app')
@section('contents')
    <div class="app-content content ">
        <div class="content-wrapper container-xxl p-0">
            <div class="content-header row">
                <div class="content-header-left col-md-9 col-12 mb-2">
                    <div class="row breadcrumbs-top">
                        <div class="col-12">
                            <h2 class="content-header-title float-left mb-0">
                                Dulux Accredited Powder Coater Recommendation Form
                            </h2>
                            <div class="breadcrumb-wrapper">
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item">
                                        <a href="#">
                                            Alumi Shield Level 1& 2, Steel Shield
                                        </a>
                                    </li>
                                </ol>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="content-body">
                <section class="horizontal-wizard">
                    <div class="bs-stepper horizontal-wizard-example linear">
                        <div class="bs-stepper-header">
                            <div class="step " data-target="#account-details">
                                <button type="button" class="step-trigger" aria-selected="false" disabled="disabled">
                                    <span class="bs-stepper-box">1</span>
                                    <span class="bs-stepper-label">
                                        <span class="bs-stepper-title">Business Details</span>
                                        <span class="bs-stepper-subtitle">Setup Business Details</span>
                                    </span>
                                </button>
                            </div>
                            <div class="line">
                                <svg xmlns="http://www.w3.org/2000/svg" width="14" height="14" viewBox="0 0 24 24"
                                     fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round"
                                     stroke-linejoin="round" class="feather feather-chevron-right font-medium-2">
                                    <polyline points="9 18 15 12 9 6"></polyline>
                                </svg>
                            </div>
                            <div class="step " data-target="#personal-info">
                                <button type="button" class="step-trigger" aria-selected="true">
                                    <span class="bs-stepper-box">2</span>
                                    <span class="bs-stepper-label">
                                        <span class="bs-stepper-title">Key Contacts</span>
                                        <span class="bs-stepper-subtitle">Add Key Contacts</span>
                                    </span>
                                </button>
                            </div>
                            <div class="line">
                                <svg xmlns="http://www.w3.org/2000/svg" width="14" height="14" viewBox="0 0 24 24"
                                     fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round"
                                     stroke-linejoin="round" class="feather feather-chevron-right font-medium-2">
                                    <polyline points="9 18 15 12 9 6"></polyline>
                                </svg>
                            </div>
                            <div class="step" data-target="#address-step">
                                <button type="button" class="step-trigger" aria-selected="false" disabled="disabled">
                                    <span class="bs-stepper-box">3</span>
                                    <span class="bs-stepper-label">
                                        <span class="bs-stepper-title">Business Profile</span>
                                        <span class="bs-stepper-subtitle">Add Business Profile</span>
                                    </span>
                                </button>
                            </div>
                            <div class="line">
                                <svg xmlns="http://www.w3.org/2000/svg" width="14" height="14" viewBox="0 0 24 24"
                                     fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round"
                                     stroke-linejoin="round" class="feather feather-chevron-right font-medium-2">
                                    <polyline points="9 18 15 12 9 6"></polyline>
                                </svg>
                            </div>
                            <div class="step" data-target="#social-links">
                                <button type="button" class="step-trigger" aria-selected="false" disabled="disabled">
                                    <span class="bs-stepper-box">4</span>
                                    <span class="bs-stepper-label">
                                        <span class="bs-stepper-title">Business Summary</span>
                                        <span class="bs-stepper-subtitle">Add Business Summary</span>
                                    </span>
                                </button>
                            </div>
                                 <div class="line">
                                <svg xmlns="http://www.w3.org/2000/svg" width="14" height="14" viewBox="0 0 24 24"
                                     fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round"
                                     stroke-linejoin="round" class="feather feather-chevron-right font-medium-2">
                                    <polyline points="9 18 15 12 9 6"></polyline>
                                </svg>
                            </div>
                            <div class="step" data-target="#account-manager">
                                <button type="button" class="step-trigger" aria-selected="false" disabled="disabled">
                                    <span class="bs-stepper-box">5</span>
                                    <span class="bs-stepper-label">
                                        <span class="bs-stepper-title">Declaration by Dulux Account Manager</span>
                                    </span>
                                </button>
                            </div>

                        </div>
                        <div class="bs-stepper-content">

                            @include('pages.company.form.first-step')
                            @include('pages.company.form.second-step')

                            <div id="address-step" class="content">
                                <div class="content-header">
                                    <h5 class="mb-0">Address</h5>
                                    <small>Enter Your Address.</small>
                                </div>
                                <form novalidate="novalidate">
                                    <div class="row">
                                        <div class="form-group col-md-6">
                                            <label class="form-label" for="address">Address</label>
                                            <input type="text" id="address" name="address" class="form-control"
                                                   placeholder="98  Borough bridge Road, Birmingham">
                                        </div>
                                        <div class="form-group col-md-6">
                                            <label class="form-label" for="landmark">Landmark</label>
                                            <input type="text" name="landmark" id="landmark" class="form-control"
                                                   placeholder="Borough bridge">
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="form-group col-md-6">
                                            <label class="form-label" for="pincode1">Pincode</label>
                                            <input type="text" id="pincode1" class="form-control" placeholder="658921">
                                        </div>
                                        <div class="form-group col-md-6">
                                            <label class="form-label" for="city1">City</label>
                                            <input type="text" id="city1" class="form-control" placeholder="Birmingham">
                                        </div>
                                    </div>
                                </form>
                                <div class="d-flex justify-content-between">
                                    <button class="btn btn-primary btn-prev waves-effect waves-float waves-light">
                                        <svg xmlns="http://www.w3.org/2000/svg" width="14" height="14"
                                             viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2"
                                             stroke-linecap="round" stroke-linejoin="round"
                                             class="feather feather-arrow-left align-middle mr-sm-25 mr-0">
                                            <line x1="19" y1="12" x2="5" y2="12"></line>
                                            <polyline points="12 19 5 12 12 5"></polyline>
                                        </svg>
                                        <span class="align-middle d-sm-inline-block d-none">Previous</span>
                                    </button>
                                    <button class="btn btn-primary btn-next waves-effect waves-float waves-light">
                                        <span class="align-middle d-sm-inline-block d-none">Next</span>
                                        <svg xmlns="http://www.w3.org/2000/svg" width="14" height="14"
                                             viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2"
                                             stroke-linecap="round" stroke-linejoin="round"
                                             class="feather feather-arrow-right align-middle ml-sm-25 ml-0">
                                            <line x1="5" y1="12" x2="19" y2="12"></line>
                                            <polyline points="12 5 19 12 12 19"></polyline>
                                        </svg>
                                    </button>
                                </div>
                            </div>
                            <div id="social-links" class="content">
                                <div class="content-header">
                                    <h5 class="mb-0">Social Links</h5>
                                    <small>Enter Your Social Links.</small>
                                </div>
                                <form novalidate="novalidate">
                                    <div class="row">
                                        <div class="form-group col-md-6">
                                            <label class="form-label" for="twitter">Twitter</label>
                                            <input type="text" id="twitter" name="twitter" class="form-control"
                                                   placeholder="https://twitter.com/abc">
                                        </div>
                                        <div class="form-group col-md-6">
                                            <label class="form-label" for="facebook">Facebook</label>
                                            <input type="text" id="facebook" name="facebook" class="form-control"
                                                   placeholder="https://facebook.com/abc">
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="form-group col-md-6">
                                            <label class="form-label" for="google">Google+</label>
                                            <input type="text" id="google" name="google" class="form-control"
                                                   placeholder="https://plus.google.com/abc">
                                        </div>
                                        <div class="form-group col-md-6">
                                            <label class="form-label" for="linkedin">Linkedin</label>
                                            <input type="text" id="linkedin" name="linkedin" class="form-control"
                                                   placeholder="https://linkedin.com/abc">
                                        </div>
                                    </div>
                                </form>
                                <div class="d-flex justify-content-between">
                                    <button class="btn btn-primary btn-prev waves-effect waves-float waves-light">
                                        <svg xmlns="http://www.w3.org/2000/svg" width="14" height="14"
                                             viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2"
                                             stroke-linecap="round" stroke-linejoin="round"
                                             class="feather feather-arrow-left align-middle mr-sm-25 mr-0">
                                            <line x1="19" y1="12" x2="5" y2="12"></line>
                                            <polyline points="12 19 5 12 12 5"></polyline>
                                        </svg>
                                        <span class="align-middle d-sm-inline-block d-none">Previous</span>
                                    </button>
                                    <button class="btn btn-success btn-submit waves-effect waves-float waves-light">
                                        Submit
                                    </button>
                                </div>
                            </div>
                            <div id="account-manager" class="content"></div>
                        </div>
                    </div>
                </section>
            </div>
        </div>
    </div>

@endsection
@push('scripts')
    <script src="duluxs/vendors/js/forms/wizard/bs-stepper.min.js"></script>
    <script src="duluxs/vendors/js/forms/select/select2.full.min.js"></script>
    <script src="duluxs/vendors/js/forms/validation/jquery.validate.min.js"></script>
    <script src="duluxs/js/scripts/forms/form-wizard.js"></script>
@endpush
