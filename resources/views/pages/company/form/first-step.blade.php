<div id="account-details" class="content">
    <div class="content-header">
        <h5 class="mb-0">Business Details</h5>
        <small class="text-muted">Enter Your Business Details.</small>
    </div>
    <form novalidate="novalidate">
        <div class="row">
            <div class="form-group col-md-12">
                <div class="custom-control custom-checkbox">
                    <input type="checkbox" class="custom-control-input"
                           id="validationCheckBootstrap" name="is_premium">
                    <label class="custom-control-label" for="validationCheckBootstrap">Is
                        Company a Premium Company (with renewal exemption):</label>
                </div>
            </div>
            <div class="form-group col-md-6">
                <label class="form-label" for="account_number">Dulux Account Number
                    *:</label>
                <input type="text" name="account_number" id="account_number"
                       class="form-control"
                       aria-describedby="account_number-error"
                       aria-invalid="false">
                <span id="account_number-error" class="error" style="display: none;"></span>
            </div>
            <div class="form-group col-md-6">
                <label class="form-label" for="abn">ABN *:</label>
                <input type="text" name="abn" id="abn" class="form-control"
                       aria-describedby="abn-error" aria-invalid="false"><span
                    id="abn-error" class="error" style="display: none;"></span>
            </div>
        </div>
        <div class="row">
            <div class="form-group col-md-6">
                <label class="form-label" for="name">Company Name *:</label>
                <input type="text" name="name" id="name" class="form-control"
                       aria-describedby="name-error" aria-invalid="false"><span
                    id="name-error" class="error" style="display: none;"></span>
            </div>
            <div class="form-group col-md-6">
                <label class="form-label" for="trading_as">Trading As / business name
                    *:</label>
                <input type="text" name="trading_as" id="trading_as" class="form-control"
                       aria-describedby="trading_as-error" aria-invalid="false">
                <span id="trading_as-error" class="error" style="display: none;"></span>
            </div>
        </div>
        <div class="row">
            <div class="form-group col-md-6">
                <label class="form-label" for="street_address">Street Address:</label>
                <input type="text" name="street_address" id="street_address"
                       class="form-control">
            </div>
            <div class="form-group col-md-6">
                <label class="form-label" for="city">City:</label>
                <input type="text" name="city" id="city" class="form-control">
            </div>
        </div>
        <div class="row">
            <div class="form-group col-md-6">
                <label for="state">State:</label>
                <select name="state" class="form-control" id="state">
                    <option value="ACT">ACT</option>
                    <option value="NSW">NSW</option>
                    <option value="NT">NT</option>
                    <option value="QLD">QLD</option>
                    <option value="SA">SA</option>
                    <option value="TAS">TAS</option>
                    <option value="WA">WA</option>
                    <option value="VIC">VIC</option>
                </select>
            </div>
            <div class="form-group col-md-6">
                <label class="form-label" for="postcode">Postcode:</label>
                <input type="text" name="postcode" id="postcode" class="form-control">
                <span id="postcode-error" class="error" style="display: none;"></span>
            </div>
        </div>
        <div class="row">
            <div class="form-group col-md-6">
                <label class="form-label" for="contact_phone">Contact Phone:</label>
                <input type="text" name="contact_phone" id="contact_phone"
                       class="form-control">
            </div>
            <div class="form-group col-md-6">
                <label class="form-label" for="website">Website:</label>
                <input type="text" name="website" id="website" class="form-control">
            </div>
        </div>
        <div class="row">
            <div class="form-group col-md-12">
                <label class="d-block" for="validationBioBootstrap">Additional terms and conditions:</label>
                <textarea class="form-control" id="validationBioBootstrap"
                          name="additional_terms_and_conditions" rows="3"></textarea>
            </div>
        </div>
    </form>
    <div class="d-flex justify-content-between">
        <button class="btn btn-outline-secondary btn-prev waves-effect" disabled="">
            <svg xmlns="http://www.w3.org/2000/svg" width="14" height="14"
                 viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2"
                 stroke-linecap="round" stroke-linejoin="round"
                 class="feather feather-arrow-left align-middle mr-sm-25 mr-0">
                <line x1="19" y1="12" x2="5" y2="12"></line>
                <polyline points="12 19 5 12 12 5"></polyline>
            </svg>
            <span class="align-middle d-sm-inline-block d-none">Previous</span>
        </button>
        <button class="btn btn-primary btn-next waves-effect waves-float waves-light">
            <span class="align-middle d-sm-inline-block d-none">Next</span>
            <svg xmlns="http://www.w3.org/2000/svg" width="14" height="14"
                 viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2"
                 stroke-linecap="round" stroke-linejoin="round"
                 class="feather feather-arrow-right align-middle ml-sm-25 ml-0">
                <line x1="5" y1="12" x2="19" y2="12"></line>
                <polyline points="12 5 19 12 12 19"></polyline>
            </svg>
        </button>
    </div>
</div>
