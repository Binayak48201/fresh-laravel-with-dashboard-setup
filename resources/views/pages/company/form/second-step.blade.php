<div id="personal-info" class="content">
    <div class="content-header">
        <h5 class="mb-0">Business Owner or Authorised Representative</h5>
        <small>This party must be a person with authority within the Powder Coat business seeking accreditation and able
            to sign off on the terms of their business. Typically this is the business owner.</small>
    </div>
    <form novalidate="novalidate">
        <div class="row">
            <div class="form-group col-md-6">
                <label class="form-label" for="business_owner_name">Name *:</label>
                <input type="text" name="business_owner_name" id="business_owner_name" class="form-control"
                       aria-describedby="business_owner_name-error" aria-invalid="false">
                <span id="business_owner_name-error" class="error" style="display: none;"></span>
            </div>
            <div class="form-group col-md-6">
                <label class="form-label" for="business_owner_position">Position:</label>
                <select name="business_owner_position" class="form-control" id="business_owner_position">
                    @foreach($positions as $key => $position)
                        <option value="{{ $key }}">{{ $position }}</option>
                    @endforeach
                </select>
            </div>
        </div>
        <div class="row mb-2">
            <div class="form-group col-md-6">
                <label class="form-label" for="business_owner_email">Email *:</label>
                <input type="text" name="business_owner_email" id="business_owner_email" class="form-control"
                       aria-describedby="business_owner_email-error" aria-invalid="false">
                <span id="business_owner_email-error" class="error" style="display: none;"></span>
            </div>
            <div class="form-group col-md-6">
                <label class="form-label" for="business_owner_phone">Phone *:</label>
                <input type="text" name="business_owner_phone" id="business_owner_phone" class="form-control"
                       aria-describedby="business_owner_phone-error" aria-invalid="false">
                <span id="business_owner_phone-error" class="error" style="display: none;"></span>
            </div>
        </div>
        <div class="content-header mt-2">
            <h5 class="mb-0">Other Key Contacts</h5>
            <small>These parties must be authorised by the business applicant (e.g. business owner) to conduct
                auditing
                and testing if they are different from the business owner or authorised representative.</small><br>
            <small><i>* Please make sure you enter both name and email if you'd like to create this contact.</i></small>
        </div>
        <div class="row">
            <div class="form-group col-md-6">
                <label class="form-label" for="secondary_contact_name">Name *:</label>
                <input type="text" name="secondary_contact_name" id="secondary_contact_name" class="form-control"
                       aria-describedby="secondary_contact_name-error" aria-invalid="false">
                <span id="secondary_contact_name-error" class="error" style="display: none;"></span>
            </div>
            <div class="form-group col-md-6">
                <label class="form-label" for="secondary_contact_position">Position:</label>
                <select name="secondary_contact_position" class="form-control" id="secondary_contact_position">
                    @foreach($positions as $key => $position)
                        <option value="{{ $key }}">{{ $position }}</option>
                    @endforeach
                </select>
            </div>
        </div>
        <div class="row">
            <div class="form-group col-md-6">
                <label class="form-label" for="secondary_contact_email">Email *:</label>
                <input type="text" name="secondary_contact_email" id="secondary_contact_email" class="form-control"
                       aria-describedby="secondary_contact_email-error" aria-invalid="false">
                <span id="secondary_contact_email-error" class="error" style="display: none;"></span>
            </div>
            <div class="form-group col-md-6">
                <label class="form-label" for="secondary_contact_phone">Phone *:</label>
                <input type="text" name="secondary_contact_phone" id="secondary_contact_phone" class="form-control"
                       aria-describedby="secondary_contact_phone-error" aria-invalid="false">
                <span id="secondary_contact_phone-error" class="error" style="display: none;"></span>
            </div>
        </div>

    </form>
    <div class="d-flex justify-content-between">
        <button class="btn btn-primary btn-prev waves-effect waves-float waves-light">
            <svg xmlns="http://www.w3.org/2000/svg" width="14" height="14"
                 viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2"
                 stroke-linecap="round" stroke-linejoin="round"
                 class="feather feather-arrow-left align-middle mr-sm-25 mr-0">
                <line x1="19" y1="12" x2="5" y2="12"></line>
                <polyline points="12 19 5 12 12 5"></polyline>
            </svg>
            <span class="align-middle d-sm-inline-block d-none">Previous</span>
        </button>
        <button class="btn btn-primary btn-next waves-effect waves-float waves-light">
            <span class="align-middle d-sm-inline-block d-none">Next</span>
            <svg xmlns="http://www.w3.org/2000/svg" width="14" height="14"
                 viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2"
                 stroke-linecap="round" stroke-linejoin="round"
                 class="feather feather-arrow-right align-middle ml-sm-25 ml-0">
                <line x1="5" y1="12" x2="19" y2="12"></line>
                <polyline points="12 5 19 12 12 19"></polyline>
            </svg>
        </button>
    </div>
</div>
