import {createApp} from 'vue'

import Flash from './components/Flash.vue'

createApp(Flash).mount("#app")
