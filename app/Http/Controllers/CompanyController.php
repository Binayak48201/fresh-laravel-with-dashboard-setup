<?php

namespace App\Http\Controllers;

use App\Modules\Models\Company;
use Illuminate\Http\Request;

class CompanyController extends Controller
{
    public $positions = [
        '' => 'Please select',
        'Director' => 'Director',
        'Managing Director' => 'Managing Director',
        'Business Manager' => 'Business Manager',
        'Sales or Business Devpt Manager' => 'Sales or Business Devpt Manager',
        'Accountant' => 'Accountant',
        'Accounts' => 'Accounts',
        'QC Manager' => 'QC Manager',
        'General Manager' => 'General Manager',
        'Office Manager' => 'Office Manager',
        'Office (Admin) Clerk' => 'Office (Admin) Clerk',
        'Operations Manager' => 'Operations Manager',
        'Business Owner' => 'Business Owner',
        'Production Manager' => 'Production Manager',
        'Production Supervisor' => 'Production Supervisor',
        'Site Manager' => 'Site Manager',
        'Marketing' => 'Marketing',
        'Supply Chain Manager' => 'Supply Chain Manager',
        'Logistics' => 'Logistics',
        'Project Manager' => 'Project Manager',
    ];

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function index()
    {
        $positions = $this->positions;

        return view('pages.company.index', compact('positions'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param \App\Modules\Models\Company $company
     * @return \Illuminate\Http\Response
     */
    public function show(Company $company)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param \App\Modules\Models\Company $company
     * @return \Illuminate\Http\Response
     */
    public function edit(Company $company)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param \App\Modules\Models\Company $company
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Company $company)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param \App\Modules\Models\Company $company
     * @return \Illuminate\Http\Response
     */
    public function destroy(Company $company)
    {
        //
    }
}
